# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem : SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """

    stack = util.Stack()

    # Push tuple with position and actions
    stack.push((problem.getStartState(), []))

    # Array of all visited nodes
    visited = []

    # Loop over the stack until it is empty
    while not stack.isEmpty():

        # Get the first node from the stack
        state, actions = stack.pop()

        # if this node is already visited go to the next node on the stack
        if state in visited:
            continue

        # Add node to the visited array
        visited.append(state)

        # If the node is the goal node, then return all actions performed to get to this node
        if problem.isGoalState(state):
            return actions

        # Get all neighbors of the current node and add them to the stack with the actions needed to get to this node
        neighbors = problem.getSuccessors(state)

        for nState, nAction, nCost in neighbors:
            if nState not in visited:
                nActions = actions + [nAction]
                stack.push( (nState, nActions) )

    return []

def breadthFirstSearch(problem):
    """

    Search the shallowest nodes in the search tree first.

    This the opposite of DFS you just change the stack with a queue
    """

    queue = util.Queue()

    # Push tuple with position and actions
    queue.push((problem.getStartState(), []))

    # Array of all visited nodes
    visited = []

    # Loop over the queue until it is empty
    while not queue.isEmpty():

        # Get the first node from the queue
        state, actions = queue.pop()

        # if this node is already visited go to the next node on the queue
        if state in visited:
            continue

        # Add node to the visited array
        visited.append(state)

        # If the node is the goal node, then return all actions performed to get to this node
        if problem.isGoalState(state):
            return actions

        # Get all neighbors of the current node and add them to the queue with the actions needed to get to this node
        neighbors = problem.getSuccessors(state)

        for nState, nAction, nCost in neighbors:
            nActions = actions + [nAction]
            queue.push( (nState, nActions) )

    return []


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    queue = util.PriorityQueue()

    # Push tuple with position and actions
    queue.push((problem.getStartState(), [], 0) , 0)

    # Array of all visited nodes
    visited = []

    # Loop over the queue until it is empty
    while not queue.isEmpty():

        # Get the first node from the queue
        state, actions, pathCost = queue.pop()

        # if this node is already visited go to the next node on the queue
        if state in visited:
            continue

        # Add node to the visited array
        visited.append(state)

        # If the node is the goal node, then return all actions performed to get to this node
        if problem.isGoalState(state):
            return actions

        # Get all neighbors of the current node and add them to the queue with the actions needed to get to this node
        neighbors = problem.getSuccessors(state)

        for nState, nAction, nCost in neighbors:
            nActions = actions + [nAction]
            nPathCost = pathCost + nCost
            queue.push( (nState, nActions, nPathCost), nPathCost )

    return []

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    queue = util.PriorityQueue()

    # Push tuple with position and actions
    queue.push((problem.getStartState(), [], 0) , 0)

    # Array of all visited nodes
    visited = []

    # Loop over the queue until it is empty
    while not queue.isEmpty():

        # Get the first node from the queue
        state, actions, pathCost = queue.pop()

        # if this node is already visited go to the next node on the queue
        if state in visited:
            continue

        # Add node to the visited array
        visited.append(state)

        # If the node is the goal node, then return all actions performed to get to this node
        if problem.isGoalState(state):
            return actions

        # Get all neighbors of the current node and add them to the queue with the actions needed to get to this node
        neighbors = problem.getSuccessors(state)

        for nState, nAction, nCost in neighbors:
            nActions = actions + [nAction]
            nPathCost = pathCost + nCost
            if nState not in visited:
                queue.push( (nState, nActions, nPathCost), nPathCost + heuristic(nState, problem) )

    return []

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
